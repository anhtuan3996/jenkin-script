namespace :node do
    desc "Run node install task"
    task :install do
        on roles(:web) do
            within release_path do
                execute :npm, "install"
            end
        end
    end

    desc "Run node build on dev task"
    task :build_dev do
        on roles(:web) do
            within release_path do
                execute :npm, "run dev"
            end
        end
    end

    desc "Run node build on production task"
    task :build_production do
        on roles(:web) do
            within release_path do
                execute :npm, "run prod"
            end
        end
    end
end