namespace :laravel do
    
    desc "Run artisan migrate task"
    task :migrate do
        on roles(:web) do
            within release_path do
              execute :php, "artisan migrate --path=database/migrations", "--force"
            end
        end
    end

    desc "Run Artisan db:seed task"
    task :seed do
        on roles(:web) do
            within release_path do
                execute :php, "artisan db:seed", "--force"
            end
        end
    end

    desc "Run artisan clear cache task"
    task :clear_cache do
        on roles(:web) do
            within release_path do
                execute :php, "artisan cache:clear"
            end
        end
    end

    desc "Run artisan clear config task"
    task :clear_config do
        on roles(:web) do
            within release_path do
                execute :php, "artisan config:clear"
            end
        end
    end

    desc "Run artisan clear view task"
    task :clear_view do
        on roles(:web) do
            within release_path do
                execute :php, "artisan view:clear"
            end
        end
    end

    desc "Run artisan clear route task"
    task :clear_route do
        on roles(:web) do
            within release_path do
                execute :php, "artisan route:clear"
            end
        end
    end

    desc "Run artisan clear-complied and optimize task"
    task :optimize do
        on roles(:web) do
            within release_path do
                execute :php, "artisan clear-compiled"
                execute :php, "artisan optimize"
            end
        end
    end

    desc "Give more permission for storage and bootstrap directories"
    task :permission do
        on roles(:web) do
            within release_path do
                # execute :chmod, "-R ug+rwx storage"
                execute :chmod, "-R a+rwx storage"
                execute :chmod, "-R a+rwx storage/logs"
                # execute :chmod, "-R ug+rwx storage/logs"
                # execute :chmod, "-R ug+rwx bootstrap"
                execute :chmod, "-R a+rwx bootstrap"
                execute :chmod, "-R ug+rwx bootstrap/cache"
                execute :chown, "-RH ec2-user:apache storage"
                execute :chown, "-RH ec2-user:apache storage/logs"
                execute :chown, "-RH ec2-user:apache bootstrap"
                execute :chown, "-RH ec2-user:apache bootstrap/cache"
            end
        end
    end

end